const { resolve } = require('path');
const isDevelopment = process.env.NODE_ENV !== 'production';

const config = {
    mode: 'development',
    target: 'node',
    entry: {
        main: resolve('./src/main.ts')
    },
    node: {
      __filename: false,
      __dirname: false
    },
    output: {
      filename:'app.js',
      path:resolve(__dirname,'../dist')
    },
    module: {
        rules: [
          {
            test: /\.tsx?$/,
            loader: ['awesome-typescript-loader?module=es6'],
            exclude: [/node_modules/]
          },
          {
            test: /\.js$/,
            loader: 'source-map-loader',
            enforce: 'pre'
          }
        ]
      },
      resolve: {
        extensions: ['.js', '.ts', '.tsx']
      }
}

module.exports = config