import express from "express";
import {resolve} from "path";
const app = express();
const port = 8080;

app.set("port",process.env.PORT || port);
app.use(express.static(resolve(__dirname,"webroot")));

const server = app.listen(app.get("port"),()=>{
    console.log("App is running at http://localhost:%d in %s mode", app.get("port"),app.get("env"));
    console.log(resolve(__dirname,"webroot"));
    console.log(__dirname);
});
export default server;