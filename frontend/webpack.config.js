const { resolve } = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const isDevelopment = process.env.NODE_ENV !== 'production';

const config = {
    mode: 'development',
    entry: {
        main: resolve('./src/main.ts')
    },
    output: {
        path:resolve(__dirname,'../dist/webroot')
    },
    module: {
        rules: [
          {
            test: /\.tsx?$/,
            loader: ['awesome-typescript-loader?module=es6'],
            exclude: [/node_modules/]
          },
          {
            test: /\.js$/,
            loader: 'source-map-loader',
            enforce: 'pre'
          },
          {
            test: /\.html$/,
            use: [
              {
                loader: 'html-loader',
                options: { minimize: !isDevelopment }
              }
            ]
          }
        ]
      },
      resolve: {
        extensions: ['.js', '.ts', '.tsx']
      },
      plugins: [
          new HtmlWebPackPlugin({
              template: './resources/index.html',
              filename: './index.html'
          }),
          new CopyWebpackPlugin([
            { from: 'resources', to: './' }
          ])
      ]
      
}

module.exports = config